package com.droidmentor.locationhelper;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private ArrayList<String> mDataset;
    private ArrayList<Integer> mIds;
    private ArrayList<Integer> mAmount;
    private ArrayList<String> mDate;
    String type;
    View.OnClickListener mClickListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public ViewHolder(CardView v) {
            super(v);
            mCardView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(ArrayList<String> myDataset, ArrayList<Integer> myIds, ArrayList<Integer> myAmount, ArrayList<String> myDate, String myType, View.OnClickListener lst) {
        mDataset = myDataset;
        mIds=myIds;
        mAmount=myAmount;
        mDate=myDate;
        type=myType;
        mClickListener=lst;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_layout, parent, false);

        ViewHolder vh = new ViewHolder(v);
        if(type.equals("products"))
            return vh;
        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);
            }
        });
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        if(type.equals("products")) {
            TextView title = (TextView) holder.mCardView.findViewById(R.id.title);
            title.setText(mDataset.get(position));

            ImageView image = (ImageView) holder.mCardView.findViewById(R.id.image);
            image.setImageResource(mIds.get(position));

            TextView descr = (TextView) holder.mCardView.findViewById(R.id.description);
            descr.setText("Amount:" + mAmount.get(position) + "                                   " + "Exp Date:" + mDate.get(position));
        }else{
            TextView title = (TextView) holder.mCardView.findViewById(R.id.title);
            title.setText(mDataset.get(position));

            ImageView image = (ImageView) holder.mCardView.findViewById(R.id.image);
            image.setImageResource(mIds.get(position));

            TextView descr = (TextView) holder.mCardView.findViewById(R.id.description);
            descr.setText(mDate.get(position).substring(0,50)+"...");
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
