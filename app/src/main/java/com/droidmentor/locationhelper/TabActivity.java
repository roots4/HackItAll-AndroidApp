package com.droidmentor.locationhelper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Random;

public class TabActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private FloatingActionButton fab;
    private Handler handler;
    private Runnable runnableCode;

    private Handler handler2;
    private Runnable runnableCode2;

    private Handler handler3;
    private Runnable runnableCode3;
    static SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    boolean isPause=false;
    static RecyclerView.Adapter mAdapter1;
    static RecyclerView.Adapter mAdapter2;
    private TabLayout tabLayout;

    static ArrayList<String> recipes1=new ArrayList<>();
    static ArrayList<String> recipes2=new ArrayList<>();
    static ArrayList<Integer> ids1=new ArrayList<>();
    static ArrayList<Integer> ids2=new ArrayList<>();
    static ArrayList<String> dates1=new ArrayList<>();
    static ArrayList<String> dates2=new ArrayList<>();
    static ArrayList<Integer> amounts1=new ArrayList<>();
    static ArrayList<Integer> amounts2=new ArrayList<>();
    static ArrayList<String> sendID=new ArrayList<>();

    private NotificationManager notifManager;

    public void createNotification(String aMessage) {
        final int NOTIFY_ID = 1002;

        // There are hardcoding only for show it's just strings
        String name = "my_package_channel";
        String id = "my_package_channel_1"; // The user-visible name of the channel.
        String description = "my_package_first_channel"; // The user-visible description of the channel.

        Intent intent;
        PendingIntent pendingIntent;
        NotificationCompat.Builder builder;

        if (notifManager == null) {
            notifManager =
                    (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notifManager.getNotificationChannel(id);
            if (mChannel == null) {
                mChannel = new NotificationChannel(id, name, importance);
                mChannel.setDescription(description);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notifManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(this);

            intent = new Intent(this, TabActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            builder.setContentTitle(aMessage)  // required
                    .setSmallIcon(android.R.drawable.ic_popup_reminder) // required
                    .setContentText(this.getString(R.string.app_name))  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(aMessage)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        } else {

            builder = new NotificationCompat.Builder(this);

            intent = new Intent(this, TabActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            builder.setContentTitle(aMessage)                           // required
                    .setSmallIcon(android.R.drawable.ic_popup_reminder) // required
                    .setContentText(this.getString(R.string.app_name))  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(aMessage)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH);
        } // else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

        Notification notification = builder.build();
        notifManager.notify(NOTIFY_ID, notification);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);


        sharedPref = this.getSharedPreferences("mine",Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        if(sharedPref.getInt("ID",0)==0) {
            Random r = new Random();
            int id = r.nextInt(1000);
            editor.putInt("ID",id).commit();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.getTabAt(0).setText("Recipes");
        tabLayout.getTabAt(1).setText("Products");

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(TabActivity.this,MyLocationUsingLocationAPI.class);
                startActivity(intent);
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position==0) {
                    Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
                    myToolbar.setTitle("Recipes");

                    CoordinatorLayout.LayoutParams params =
                            (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
                    FloatingActionButton.Behavior behavior =
                            (FloatingActionButton.Behavior) params.getBehavior();

                    if (behavior != null) {
                        behavior.setAutoHideEnabled(false);
                    }

//                    findViewById(R.id.description).setVisibility(View.VISIBLE);

                    fab.hide();
                } else if (position==1) {
                    Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
                    myToolbar.setTitle("Products");

                    fab.show();

                    CoordinatorLayout.LayoutParams params =
                            (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
                    FloatingActionButton.Behavior behavior =
                            (FloatingActionButton.Behavior) params.getBehavior();

//                    findViewById(R.id.description).setVisibility(View.GONE);

                    if (behavior != null) {
                        behavior.setAutoHideEnabled(true);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });




        // Create the Handler object (on the main thread by default)
        handler = new Handler();
        // Define the code block to be executed
        runnableCode = new Runnable() {
            @Override
            public void run() {
                if(isPause){
                    handler.removeCallbacks(runnableCode);
                }else {
                    // Do something here on the main thread
                    Log.d("Handlers", "Called on main thread");

                    AsyncR retProducts = new AsyncR();
                    retProducts.CallServiceTask(TabActivity.this);
                    retProducts.execute("products", sharedPref.getInt("ID",0)+"");

                    // Repeat this the same runnable code block again another 2 seconds
                    handler.postDelayed(runnableCode, 2000);
                }
            }
        };

        handler.post(runnableCode);

        // Create the Handler object (on the main thread by default)
        handler3 = new Handler();
        // Define the code block to be executed
        runnableCode3 = new Runnable() {
            @Override
            public void run() {
                if(isPause){
                    handler3.removeCallbacks(runnableCode3);
                }else {
                    // Do something here on the main thread
                    Log.d("Handlers", "Called on main thread");
                    AsyncR retRecipes = new AsyncR();
                    retRecipes.CallServiceTask(TabActivity.this);
                    retRecipes.execute("recipes", sharedPref.getInt("ID",0)+"");

                    // Repeat this the same runnable code block again another 2 seconds
                    handler3.postDelayed(runnableCode3, 2000);
                }
            }
        };

        // Start the initial runnable task by posting through the handler
        handler3.post(runnableCode3);

        // Create the Handler object (on the main thread by default)
        handler2 = new Handler();
        // Define the code block to be executed
        runnableCode2 = new Runnable() {
            @Override
            public void run() {
                if(isPause){
                    handler2.removeCallbacks(runnableCode2);
                }else {
                    // Do something here on the main thread
                    Log.d("Handlers", "Called on main thread");

                    AsyncRecFinder recF = new AsyncRecFinder();
                    recF.CallServiceTask(TabActivity.this);
                    recF.execute(sharedPref.getInt("ID",0)+"");

                    // Repeat this the same runnable code block again another 2 seconds
                    handler2.postDelayed(runnableCode2, 2000);
                }
            }
        };
        handler2.post(runnableCode2);
    }

    public void onFinishEverything(String result){
        Log.d("Result ", result);
        if(result!=null && !result.equals("null"))
            createNotification("Your have joined a meal! Enjoy:)!");
    }

    public void onFinishReceiveRecipes(String result) {
        if(result!=null && !result.equals("null")) {
            try {
                JSONObject res = new JSONObject(result);
                JSONArray products = res.getJSONArray("recipes");

//                recipes1.removeAll(recipes1);
//                dates1.removeAll(dates1);
//                sendID.removeAll(sendID);
//                ids1.removeAll(ids1);
                for (int i = 0; i < products.length(); i++) {
                    String name = products.getJSONObject(i).getString("recipeName");
                    if(!recipes1.contains(name)) {
                        recipes1.add(name);
                        dates1.add(products.getJSONObject(i).getString("description"));
                        sendID.add(products.getJSONObject(i).getString("recipeGlobalId"));
                        try {
                            Class cls = R.drawable.class;
                            Field field = cls.getField("a" + products.getJSONObject(i).getString("recipeGlobalId"));
                            int id = field.getInt(null);
                            ids1.add(id);
                            mAdapter1.notifyItemInserted(i);
                        } catch (Exception e) {
                            Log.e("MyTag", "Failure to get drawable id.", e);
                        }
                    }
                    mAdapter1.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Get: ", result);
        }
    }

    public void onFinishReceiveProducts(String result){
        if(result!=null && !result.equals("null")) {
            try {
                JSONObject res = new JSONObject(result);
                JSONArray products = res.getJSONArray("products");

//                recipes2.removeAll(recipes2);
//                amounts2.removeAll(amounts2);
//                dates2.removeAll(dates2);
//                ids2.removeAll(ids2);
                for (int i = 0; i < products.length(); i++) {
                    String name = products.getJSONObject(i).getString("productName");
                    if(!recipes2.contains(name)) {
                        recipes2.add(name);
                        amounts2.add(Integer.parseInt(products.getJSONObject(i).getString("quantity")));
                        dates2.add(products.getJSONObject(i).getString("expDate"));
                        try {
                            Class cls = R.drawable.class;
                            Field field = cls.getField(name.toLowerCase());
                            int id = field.getInt(null);
                            ids2.add(id);
                            mAdapter2.notifyItemInserted(i);
                        } catch (Exception e) {
                            Log.e("MyTag", "Failure to get drawable id.", e);
                        }
                    }
                    mAdapter2.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Get: ", result);
        }
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_tab, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.content_scrolling, container, false);
//            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

            final RecyclerView mRecyclerView1;
            RecyclerView.LayoutManager mLayoutManager1;
            final RecyclerView mRecyclerView2;
            RecyclerView.LayoutManager mLayoutManager2;

            ArrayList<String> show_recipes=null;
            ArrayList<Integer> show_ids=null;
            ArrayList<String> show_dates=null;
            ArrayList<Integer> show_amounts=null;
            String type;

            if( getArguments().getInt(ARG_SECTION_NUMBER)%2==1){
                show_recipes=recipes1;
                show_ids=ids1;
                show_amounts=amounts1;
                show_dates=dates1;
                type="recipes";
            }else {
                show_recipes=recipes2;
                show_ids=ids2;
                show_amounts=amounts2;
                show_dates=dates2;
                type="products";
            }

            if( getArguments().getInt(ARG_SECTION_NUMBER)%2==1) {

                mRecyclerView1 = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
                mRecyclerView1.setNestedScrollingEnabled(false);


                // use this setting to improve performance if you know that changes
                // in content do not change the layout size of the RecyclerView
                mRecyclerView1.setHasFixedSize(true);

                // use a linear layout manager
                mLayoutManager1 = new LinearLayoutManager(getActivity());
                mRecyclerView1.setLayoutManager(mLayoutManager1);

                final ArrayList<Integer> send_ids=ids1;
                final ArrayList<String> send_name=recipes1;
                final ArrayList<String> send_desc=dates1;
                final ArrayList<String> send_recId=sendID;

                // specify an adapter (see also next example)
                mAdapter1 = new MyAdapter(show_recipes, show_ids, show_amounts, show_dates, type,new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int itemPosition = mRecyclerView1.indexOfChild(v);
                        Intent intent=new Intent(getActivity(),DetailActivity.class);
                        intent.putExtra("image",send_ids.get(itemPosition));
                        intent.putExtra("recid",send_recId.get(itemPosition));
                        intent.putExtra("name",send_name.get(itemPosition));
                        intent.putExtra("desc",send_desc.get(itemPosition));
                        intent.putExtra("user",sharedPref.getInt("ID",0));
                        startActivity(intent);
                    }
                });
                mRecyclerView1.setAdapter(mAdapter1);
                mRecyclerView1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int itemPosition = mRecyclerView1.indexOfChild(v);
                        Log.d("Tagsdkasjkfashjsahf", String.valueOf(itemPosition));

                    }
                });
            } else{
                mRecyclerView2 = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
                mRecyclerView2.setNestedScrollingEnabled(false);


                // use this setting to improve performance if you know that changes
                // in content do not change the layout size of the RecyclerView
                mRecyclerView2.setHasFixedSize(true);

                // use a linear layout manager
                mLayoutManager2 = new LinearLayoutManager(getActivity());
                mRecyclerView2.setLayoutManager(mLayoutManager2);
                // specify an adapter (see also next example)
                mAdapter2 = new MyAdapter(show_recipes,show_ids,show_amounts,show_dates,type, null);
                mRecyclerView2.setAdapter(mAdapter2);
            }

            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(runnableCode, 1000);
        isPause=false;
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        isPause=true;
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        isPause=true;
    }
}
