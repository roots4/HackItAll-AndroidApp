package com.droidmentor.locationhelper;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import butterknife.BindView;

public class DetailActivity extends AppCompatActivity {

    String name;
    String desc;
    int id;
    int user;
    String recid;

    String choice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        id = intent.getIntExtra("image",0);
        name = intent.getStringExtra("name");
        desc = intent.getStringExtra("desc");
        user = intent.getIntExtra("user",0);
        recid = intent.getStringExtra("recid");

        CollapsingToolbarLayout ct=(CollapsingToolbarLayout)findViewById(R.id.toolbar_layout);
        TextView text=(TextView)findViewById(R.id.descriere);
        TextView detail=(TextView)findViewById(R.id.detail_title);

        Drawable drawable = getResources().getDrawable(id);
        ct.setBackground(drawable);
        text.setText(desc);
        detail.setText(name);

        setTitle("");

        final Button join=(Button)findViewById(R.id.btnjoin);
        final Button diy=(Button)findViewById(R.id.btndiy);

        join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                join.setBackground(getResources().getDrawable(R.drawable.bg_proceed_enable));
                diy.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
                choice="join";
                AsyncTRecipe send=new AsyncTRecipe();
                send.execute(String.valueOf(user), recid);
                finish();
            }
        });
        diy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                join.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
                diy.setBackground(getResources().getDrawable(R.drawable.bg_proceed_enable));
                choice="diy";
                finish();
            }
        });
    }
}
