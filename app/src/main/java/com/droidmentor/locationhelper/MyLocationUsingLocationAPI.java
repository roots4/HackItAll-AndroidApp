package com.droidmentor.locationhelper;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.droidmentor.locationhelper.LocationUtil.PermissionUtils;
import com.droidmentor.locationhelper.LocationUtil.PermissionUtils.PermissionResultCallback;
import com.droidmentor.locationhelper.camera.CameraPreview;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import android.hardware.Camera;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class MyLocationUsingLocationAPI extends AppCompatActivity implements ConnectionCallbacks,
        OnConnectionFailedListener,OnRequestPermissionsResultCallback, OnMapReadyCallback,
        PermissionResultCallback,DatePickerDialog.OnDateSetListener {


//    @BindView(R.id.cutremur)Button cutremur;
//    @BindView(R.id.avalansa)Button avalansa;
//    @BindView(R.id.nuclear)Button nuclear;
//    @BindView(R.id.foc)Button foc;

    @BindView(R.id.desc)EditText desc;
    @BindView(R.id.amount)EditText amount;

    @BindView(R.id.btnLocation)Button btnProceed;
    @BindView(R.id.tvAddress)TextView tvAddress;
    @BindView(R.id.rlPickLocation)RelativeLayout rlPick;


    // LogCat tag
    private static final String TAG = MyLocationUsingLocationAPI.class.getSimpleName();

    private final static int PLAY_SERVICES_REQUEST = 1000;
    private final static int REQUEST_CHECK_SETTINGS = 2000;
    private static final int RC_HANDLE_GMS = 9001;

    private Camera mCamera;
    private CameraPreview mPreview;

    boolean canTakePicture=true;

    private DatePickerDialog datePickerDialog;

    private Location mLastLocation;

    // Google client to interact with Google API

    private GoogleApiClient mGoogleApiClient;

    private GoogleMap mMap;

    private Marker position=null;

    String currentLocation;
    String danger;

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    double latitude;
    double longitude;

    String date;
    // list of permissions

    ArrayList<String> permissions=new ArrayList<>();
    PermissionUtils permissionUtils;

    boolean isPermissionGranted;


    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            JSONObject json = new JSONObject();

            String encodedImage = Base64.encodeToString(data, Base64.DEFAULT);

            AsyncT send_task=new AsyncT();

            try {
                json.put("Lat", latitude);
                json.put("Long", longitude);
                json.put("Desc", desc.getText());
                json.put("Danger", danger);
                json.put("Address", currentLocation.replaceAll("ș","s")
                .replaceAll("ț","t")
                .replaceAll("î","i")
                .replaceAll("ă","a")
                .replaceAll("â","a")
                .replaceAll("Ș","S")
                .replaceAll("Ț","T")
                .replaceAll("Î","I")
                .replaceAll("Ă","A")
                .replaceAll("Â","A"));
                json.put("Photo",encodedImage);
                Log.d("picture and stuff", json.toString());
                send_task.execute(json);

//                // Start NewActivity.class
//                Intent myIntent = new Intent(MyLocationUsingLocationAPI.this,
//                        AddActivity.class);
//                startActivity(myIntent);

                mCamera.startPreview();


            }catch (JSONException e){
                Log.d(TAG, "JSON Problem: " + e.getMessage());
            }

            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null){
                Log.d(TAG, "Error creating media file, check storage permissions: ");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                mCamera.startPreview();
                canTakePicture=true;
            } catch (FileNotFoundException e) {
                Log.d(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, "Error accessing file: " + e.getMessage());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_service);

        ButterKnife.bind(this);

        permissionUtils=new PermissionUtils(MyLocationUsingLocationAPI.this);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissions.add(Manifest.permission.CAMERA);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);

        permissionUtils.check_permission(permissions,"Need GPS permission for getting your location",1);

//        cutremur.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                cutremur.setBackground(getResources().getDrawable(R.drawable.bg_proceed_enable));
//                avalansa.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                nuclear.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                foc.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                danger="Cutremur";
//            }
//        });
//        avalansa.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                cutremur.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                avalansa.setBackground(getResources().getDrawable(R.drawable.bg_proceed_enable));
//                nuclear.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                foc.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                danger="Avalansa";
//            }
//        });
//        foc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                cutremur.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                avalansa.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                nuclear.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                foc.setBackground(getResources().getDrawable(R.drawable.bg_proceed_enable));
//                danger="Foc";
//            }
//        });
//        nuclear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                cutremur.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                avalansa.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                nuclear.setBackground(getResources().getDrawable(R.drawable.bg_proceed_enable));
//                foc.setBackground(getResources().getDrawable(R.drawable.bg_proceed_disable));
//                danger="Razboi Nuclear";
//            }
//        });



        rlPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLocation();

                if (mLastLocation != null) {
                    latitude = mLastLocation.getLatitude();
                    longitude = mLastLocation.getLongitude();

                    getAddress();

                    putDouble(editor,getString(R.string.location_lat), latitude).commit();
                    putDouble(editor,getString(R.string.location_long), longitude).commit();
                    editor.putString(getString(R.string.address), currentLocation).commit();

                    addMarker(latitude,longitude);

                } else {

//                    if(btnProceed.isEnabled())
//                        btnProceed.setEnabled(false);

                    showToast("Couldn't get the location. Make sure location is enabled on the device");
                }
            }
        });

        // check availability of play services
        if (checkPlayServices()) {

            sharedPref = this.getSharedPreferences("mine",Context.MODE_PRIVATE);
            editor = sharedPref.edit();

            // Building the GoogleApi client
            buildGoogleApiClient();
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

//            start_camera();
        }

        datePickerDialog = new DatePickerDialog(
                this, MyLocationUsingLocationAPI.this, 2018, 3, 24);

        ((Button) findViewById(R.id.date))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        datePickerDialog.show();
                    }
                });

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sendStuff();
                finish();
                showToast("Event Sent");
            }
        });

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Button btn = (Button)findViewById(R.id.date);
        btn.setText(dayOfMonth+"-"+month+"-"+year);
        date=checkDigit(dayOfMonth)+"/"+checkDigit(month)+"/"+year;
        Log.d("Date: ",year+" "+month+" "+dayOfMonth);
    }

    public String checkDigit(int number)
    {
        return number<=9?"0"+number:String.valueOf(number);
    }

    public void sendStuff() {

        JSONObject json = new JSONObject();

        AsyncT send_task=new AsyncT();

        try {
            json.put("lat", latitude);
            json.put("long", longitude);
            json.put("productName", desc.getText());
            json.put("quantity", amount.getText());
            json.put("expDate", date);
            json.put("userId", sharedPref.getInt("ID",0)+"");
            json.put("productId","2");
            json.put("address", currentLocation.replaceAll("ș","s")
                    .replaceAll("ț","t")
                    .replaceAll("î","i")
                    .replaceAll("ă","a")
                    .replaceAll("â","a")
                    .replaceAll("Ș","S")
                    .replaceAll("Ț","T")
                    .replaceAll("Î","I")
                    .replaceAll("Ă","A")
                    .replaceAll("Â","A"));
            Log.d("picture and stuff", json.toString());
            send_task.execute(json);

//                // Start NewActivity.class
//                Intent myIntent = new Intent(MyLocationUsingLocationAPI.this,
//                        AddActivity.class);
//                startActivity(myIntent);

        }catch (JSONException e){
            Log.d(TAG, "JSON Problem: " + e.getMessage());
        }
    }

    SharedPreferences.Editor putDouble(final SharedPreferences.Editor edit, final String key, final double value) {
        return edit.putLong(key, Double.doubleToRawLongBits(value));
    }

    double getDouble(final SharedPreferences prefs, final String key, final double defaultValue) {
        return Double.longBitsToDouble(prefs.getLong(key, Double.doubleToLongBits(defaultValue)));
    }

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private void start_camera(){
        // Create an instance of Camera
        mCamera = getCameraInstance();
        mCamera.setDisplayOrientation(90);

        Camera.Parameters params = mCamera.getParameters();
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        mCamera.setParameters(params);

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
//        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
//        preview.addView(mPreview);
    }

    /**
     * Method to display the location on UI
     * */

    private void getLocation() {

        if (isPermissionGranted) {

            try
            {
                mLastLocation = LocationServices.FusedLocationApi
                        .getLastLocation(mGoogleApiClient);

            }
            catch (SecurityException e)
            {
                e.printStackTrace();
            }

        }

    }

    void addMarker(double lat, double lng){
        // Add a marker in Sydney and move the camera
        LatLng location = new LatLng(lat, lng);

        if(position==null) {
            position = mMap.addMarker(new MarkerOptions().position(location).title(currentLocation));
            position.setTitle(currentLocation);
            position.showInfoWindow();
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position.getPosition(), 14));
        }else{
            position.setPosition(location);
            position.setTitle(currentLocation);
            position.showInfoWindow();
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position.getPosition(), 14));
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        getLocation();

        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();

            getAddress();

            putDouble(editor,getString(R.string.location_lat), latitude).commit();
            putDouble(editor,getString(R.string.location_long), longitude).commit();
            editor.putString(getString(R.string.address), currentLocation).commit();

            addMarker(latitude,longitude);

        } else {

//            if(btnProceed.isEnabled())
//                btnProceed.setEnabled(false);

            latitude = getDouble(sharedPref,getString(R.string.location_lat), 44.433343f);
            longitude = getDouble(sharedPref,getString(R.string.location_long), 26.097780f);
            currentLocation = sharedPref.getString(getString(R.string.address),"");

            addMarker(latitude,longitude);
        }



    }

    public Address getAddress(double latitude,double longitude)
    {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude,longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            return addresses.get(0);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }


    public void getAddress()
    {

        Address locationAddress=getAddress(latitude,longitude);

        if(locationAddress!=null)
        {
            String address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();

            String localCurrentLocation;

            if(!TextUtils.isEmpty(address))
            {
                localCurrentLocation =address;
                currentLocation =address;

                if (!TextUtils.isEmpty(address1)) {
                    localCurrentLocation += "\n" + address1;
                    currentLocation += "\n" + address1;
                }

                if (!TextUtils.isEmpty(city))
                {
                    localCurrentLocation +="\n"+city;

                    if (!TextUtils.isEmpty(postalCode))
                        localCurrentLocation +=" - "+postalCode;
                }
                else
                {
                    if (!TextUtils.isEmpty(postalCode))
                        localCurrentLocation +="\n"+postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    localCurrentLocation +="\n"+state;

                if (!TextUtils.isEmpty(country))
                    localCurrentLocation +="\n"+country;

                tvAddress.setText(localCurrentLocation);
                tvAddress.setVisibility(View.GONE);

//                if(!btnProceed.isEnabled())
//                    btnProceed.setEnabled(true);


            }

        }

    }

    /**
     * Creating google api client object
     * */

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        mGoogleApiClient.connect();

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {

                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location requests here
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MyLocationUsingLocationAPI.this, REQUEST_CHECK_SETTINGS);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });


    }




    /**
     * Method to verify google play services on the device
     * */

    private boolean checkPlayServices() {

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();

        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(resultCode)) {
                googleApiAvailability.getErrorDialog(this,resultCode,
                        PLAY_SERVICES_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        getLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        break;
                    default:
                        break;
                }
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
//        mCamera.startPreview();
//        mCamera.startPreview();
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
//        mCamera.stopPreview();
//        mCamera.release();
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
//        mCamera.stopPreview();
//        mCamera.release();
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        getLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }


    // Permission check functions


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // redirects to utils
        permissionUtils.onRequestPermissionsResult(requestCode,permissions,grantResults);

    }




    @Override
    public void PermissionGranted(int request_code) {
        Log.i("PERMISSION","GRANTED");
        isPermissionGranted=true;
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        Log.i("PERMISSION PARTIALLY","GRANTED");
    }

    @Override
    public void PermissionDenied(int request_code) {
        Log.i("PERMISSION","DENIED");
    }

    @Override
    public void NeverAskAgain(int request_code) {
        Log.i("PERMISSION","NEVER ASK AGAIN");
    }

    public void showToast(String message)
    {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }



}

