package com.droidmentor.locationhelper;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

/**
 * Created by alin on 1/16/18.
 */

class AsyncT extends AsyncTask<JSONObject,Void,Void> {


    protected void onPostExecute(MyLocationUsingLocationAPI activity) {

        Log.d("Send data","finished");
    }

    @Override
    protected Void doInBackground(JSONObject... params) {

        try {
            URL url = new URL("http://35.204.246.106:80/services/addUserProduct/"); //Enter URL here
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST"); // here you are telling that it is a POST request, which can be changed into "PUT", "GET", "DELETE" etc.
            httpURLConnection.setRequestProperty("Content-Type", "application/json"); // here you are setting the `Content-Type` for the data you are sending which is `application/json`
            httpURLConnection.connect();

            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes(params[0].toString());
            wr.flush();
            wr.close();

            int statusCode = httpURLConnection.getResponseCode();

            Log.d("RESPONSE_CODE",statusCode+"");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


}

class AsyncTRecipe extends AsyncTask<String,Void,Void> {


    @Override
    protected Void doInBackground(String... params) {

        try {
            URL url = new URL("http://35.204.246.106:80/services/registerIntention/"+params[0]+"/"+params[1]); //Enter URL here
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod("GET"); // here you are telling that it is a POST request, which can be changed into "PUT", "GET", "DELETE" etc.
            httpURLConnection.connect();

            int statusCode = httpURLConnection.getResponseCode();

            Log.d("AICI RESPONSE_CODE",statusCode+"http://35.204.246.106:80/services/registerIntention/"+params[0]+"/"+params[1]);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}

class AsyncR extends AsyncTask<String,Void,Void> {

    StringBuffer response=null;
    TabActivity caller;
    String type;

    void CallServiceTask(TabActivity caller) {
        this.caller = caller;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(response!=null)
            if(type.equals("recipes"))
                caller.onFinishReceiveRecipes(response.toString());
            else
                caller.onFinishReceiveProducts(response.toString());
    }

    @Override
    protected Void doInBackground(String... params) {

        try {
            type=params[0];

            URL url;

            if(params[0]=="recipes")
                url = new URL("http://35.204.246.106:80/services/getRecipes/"+params[1]); //Enter URL here
            else
                url = new URL("http://35.204.246.106:80/services/getUserProducts/"+params[1]); //Enter URL here
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod("GET"); // here you are telling that it is a POST request, which can be changed into "PUT", "GET", "DELETE" etc.
            httpURLConnection.connect();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            int statusCode = httpURLConnection.getResponseCode();

            Log.d("RESPONSE_CODE",statusCode+"");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


}

class AsyncRecFinder extends AsyncTask<String,Void,Void> {

    StringBuffer response=null;
    TabActivity caller;
    String type;

    void CallServiceTask(TabActivity caller) {
        this.caller = caller;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (response != null)
            caller.onFinishEverything(response.toString());
    }

    @Override
    protected Void doInBackground(String... params) {

        try {
            URL url = new URL("http://35.204.246.106:80/services/verifyRecipesMatch/"+params[0]); //Enter URL here
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod("GET"); // here you are telling that it is a POST request, which can be changed into "PUT", "GET", "DELETE" etc.
            httpURLConnection.connect();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(httpURLConnection.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            int statusCode = httpURLConnection.getResponseCode();

            Log.d("RESPONSE_CODE",statusCode+"");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


}

